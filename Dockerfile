FROM debian:bullseye
RUN DEBIAN_FRONTEND=noninteractive apt-get update \
  && apt-get install -y --no-install-recommends \
      openssh-server tini \
  && apt-get clean \
  && rm -rf /var/lib/apt/lists/* \
  && rm /etc/ssh/ssh_host_*

RUN useradd sshtunnel -m -d /home/sshtunnel -s /bin/false
RUN mkdir -p /run/sshd
ADD sshd_config /etc/ssh/sshd_config

ENTRYPOINT ["/usr/bin/tini", "--"]
CMD ["/usr/sbin/sshd", "-D", "-e"]
