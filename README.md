# SSH Tunnel

Minimalistic SSHD server for TCP forwarding. You can use it for reverse SSH tunnels.

## Quick test

Public server

```bash
docker run --rm \
  -p 2222:22 \
  -p 1080:1080 \
  -v `pwd`/test_host_keys/ssh_host_ecdsa_key:/etc/ssh/ssh_host_ecdsa_key:ro \
  -v `pwd`/test_host_keys/ssh_host_ecdsa_key.pub:/etc/ssh/ssh_host_ecdsa_key.pub:ro \
  -v `pwd`/test_host_keys/ssh_host_ed25519_key:/etc/ssh/ssh_host_ed25519_key:ro \
  -v `pwd`/test_host_keys/ssh_host_ed25519_key.pub:/etc/ssh/ssh_host_ed25519_key.pub:ro \
  -v `pwd`/test_host_keys/ssh_host_rsa_key:/etc/ssh/ssh_host_rsa_key:ro \
  -v `pwd`/test_host_keys/ssh_host_rsa_key.pub:/etc/ssh/ssh_host_rsa_key.pub:ro \
  -v $HOME/.ssh/id_rsa.pub:/home/sshtunnel/.ssh/authorized_keys:ro \
  registry.gitlab.com/adam.stanek/docker-ssh-tunnel
```

The initiator (hidden, NATed client)

```bash
ssh -NR 1080:localhost:22 sshtunnel@<server> -p 2222
```

The connector (your machine, anywhere around the world)

```bash
ssh -p 1080 <server>
```

## Host keys

You can generate host keys by `ssh-keygen -A`

## Build

```bash
docker build -t registry.gitlab.com/adam.stanek/docker-ssh-tunnel .
```